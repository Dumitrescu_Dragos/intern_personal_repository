import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';

@Component({
  selector: 'app-generic-component-child',
  templateUrl: './generic-component-child.component.html',
  styleUrls: ['./generic-component-child.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class GenericComponentChildComponent implements OnInit {
  @Input() dataChild: string;
  constructor() { }

  ngOnInit() {
    console.log(this.dataChild);
  }

}
