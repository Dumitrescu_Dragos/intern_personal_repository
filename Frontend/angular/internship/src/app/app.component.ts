import { Component } from '@angular/core';

@Component({
  selector: 'app-root',//put whatever you want, but be careful about consistency-> creates new tag
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']//you can have more (that is why we have a vector)
})
export class AppComponent {
  title = 'app';//default is public
  private _numeVariabila = 'test';
  private _description: string ='Here are some links to help you start:';
  welcomeTitle: string = 'Welcome to Boss';
  info: string ='Mesaj de trimis';
}
