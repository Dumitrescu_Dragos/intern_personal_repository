import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { GenericComponentChildComponent } from './generic-component-child/generic-component-child.component';
import { GenericComponentParentComponent } from './generic-component-parent/generic-component-parent.component';
import { NewcompComponent } from './newcomp/newcomp.component';


@NgModule({
  declarations: [
    AppComponent,
    GenericComponentChildComponent, // what components we use in the application (name of components)
    GenericComponentParentComponent, NewcompComponent
  ],
  imports: [
    BrowserModule //modules that are used in the application (group of components with services, modules) imports modules
  ],
  exports:[],//use to exports modules (make them public to other modules)
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
