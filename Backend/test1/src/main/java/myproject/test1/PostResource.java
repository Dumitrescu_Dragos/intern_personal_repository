package myproject.test1;

import myproject.test1.model.Post;
import myproject.test1.repository.PostRepository;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("api/vX/posts")
public class PostResource {
    private PostRepository repo=new PostRepository();

    @POST
    @Path("{id}/{descr}/{author}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response createPost(@PathParam("id") int id,
                          @PathParam("descr") String descr,
                          @PathParam("author") String author) {
        Post output=new Post(id,descr,author);
        repo.addPost(output);

        return  Response.status(Response.Status.OK).entity(repo.getAll()).build();
    }
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAllPosts() {
        return  Response.status(Response.Status.OK).entity(repo.getAll()).build();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_XML)
    public Response findAllPosts(@PathParam("id") int id) {
        Post p=repo.getPostById(id);
        if(p==null)
            return Response.status(Response.Status.OK).entity("Error404: The id can't be found!").build();
        return  Response.status(Response.Status.OK).entity(p.toString()).build();
    }
}
