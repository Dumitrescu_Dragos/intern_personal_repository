package myproject.test1.repository;

import myproject.test1.model.Post;

import java.util.ArrayList;
import java.util.List;

public class PostRepository {
    private List<Post> posts;

    public PostRepository() {
        posts = new ArrayList<Post>();
        posts.add(new Post(1,"descr 1","dragos"));
        posts.add(new Post(2,"descr 2","dragos"));
        posts.add(new Post(3,"descr 3","dragos"));
    }

    public void addPost(Post p)
    {
        posts.add(p);
        System.out.println(getAll());
    }

    public List<Post> getAll() {
        return posts;
    }

    public Post getPostById(int id)
    {
        for(Post p: posts)
        {
            if(p.getId()==id)
                return p;
        }
        return null;
    }
}
