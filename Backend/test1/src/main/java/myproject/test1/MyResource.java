package myproject.test1;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.HashMap;
import java.util.Map;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("myresources")
public class MyResource {

    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */

    @GET
    @Path("resource1")
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt() {
        return "Hello!";
    }


    @GET
    @Path("resource2/{messageId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getJSON(@PathParam("messageId") int id) {
        Map<String,String> map=new HashMap<String, String>();
        map.put("1","abc");
        map.put("2","def");
        map.put("3","ghi");
        return Response.status(Status.OK).entity(map).build();
    }


}
